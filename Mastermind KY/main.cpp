#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <string>

using namespace std;


struct joueur{ //Creation structure joueur
    int combiSecrete[4];
    int choixJoueur[4];
    int essaiJoueur = 10;
    int partieGagne = 0;
    int partiePerdu = 0;
};

int main()
{

    srand(time(NULL)); //initialisation random
    joueur mastermind;
    bool rejouer = true;

    while (rejouer) {
        //Debut partie
        mastermind.combiSecrete[0] = rand()%10; //Initialisation des 4 numeros en random
        mastermind.combiSecrete[1] = rand()%10;
        mastermind.combiSecrete[2] = rand()%10;
        mastermind.combiSecrete[3] = rand()%10;


        cout << "L'ordinateur commence la partie!" << endl;
        cout << "-----------------" << endl;
        cout << "| ? | ? | ? | ? |" << endl;
        cout << "-----------------" << endl;
        cout << "A vous de trouver l'ordre des pions entre 0 et 9:" << endl << endl;

        mastermind.essaiJoueur=10; //initialisation nombre d'essai joueur pour chaque debut de partie

        while (mastermind.essaiJoueur > 0) { // initialisation fin de la boucle � 0 essais

            for(int i=0;i<4;i++){ // proposition joueurs 4 chiffres
                cout << "Case " << i+1 << ": ";
                cin >> mastermind.choixJoueur[i];
                if (mastermind.choixJoueur[i] > 9){
                    cout << "Veuillez taper un nombre entre 0 et 9!!!" << endl;
                }
            }
            cout << endl;


            int nbBienPlaces = 0; //Comparaison si nombre bien plac� ou pas
            int nbMalPlaces = 0;

            for (int i=0; i<4; ++i) {
                if (mastermind.combiSecrete[i]==mastermind.choixJoueur[i]) {
                    nbBienPlaces++;
                    cout << "Vous avez " << nbBienPlaces << " pions bien place!" << endl;
                } else {
                    //Test mal plac�
                    nbMalPlaces++;
                    cout << "Vous avez " << nbMalPlaces << " pions mal place!" << endl;
                }
                cout << endl;
            }

            if (nbBienPlaces==4) { //Gagn�
                mastermind.partieGagne++;
                break; //sort de la boucle while
            }

            mastermind.essaiJoueur--; // decrementation essai joueur pour chaque tour
        }


        if (!mastermind.essaiJoueur) { //Tester si perdu
            cout << "Vous avez perdu la partie!" << endl; //perdu
            mastermind.partiePerdu++;
        }
        else {
            cout << "Vous avez gagner la partie!" << endl;
            mastermind.partieGagne++;
        }


        if (!mastermind.essaiJoueur){ //Afficher gagn� et perdu CODEEEEEE

        }
        //switch....
        //Manque Demander si on veut rejouer

        rejouer = false;
    }

    return 0;
}
